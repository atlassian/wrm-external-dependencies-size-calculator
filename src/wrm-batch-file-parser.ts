import { groupBy } from 'lodash';

interface IModuleMeta {
    webresourceKey: string;
    location: string;
}

const MODULE_SEPERATOR_REGEX = /(\/\* module-key[^*]+\*\/)/;
const MODULE_META_REGEX = /module-key = '([^']+)',\s+location = '([^']+)'/;

const MODULE_LINE_IDENTIFIER = '/* module-key';

const extractMetaFromModuleKey = (moduleString: string): IModuleMeta | null => {
    const splitResult = moduleString.match(MODULE_META_REGEX);
    if (splitResult === null) {
        return null;
    }

    const [, webresourceKey, location] = splitResult;
    if (webresourceKey === null || location === null) {
        return null;
    }

    return {
        location,
        webresourceKey,
    };
};

interface IModuleDescriptor {
    location: string;
    webresourceKey: string;
    size: number;
}

export interface IGroupedModuleDescriptor {
    webresourceKey: string;
    size: number;
    modules: Array<IModuleDescriptor>;
}

export const parseWRMBatchFile = (file: string | null): Array<IModuleDescriptor> => {
    if (file === null) {
        return [];
    }

    const moduleSplits = file.split(MODULE_SEPERATOR_REGEX);
    if (moduleSplits[0].indexOf(MODULE_LINE_IDENTIFIER) !== 0) {
        moduleSplits.shift();
    }

    const modules: Array<IModuleDescriptor> = [];
    for (let i = 0; i < moduleSplits.length; i += 2) {
        const meta = extractMetaFromModuleKey(moduleSplits[i]);
        if (meta !== null) {
            const source = moduleSplits[i + 1];
            modules.push({
                location: meta.location,
                size: source.length,
                webresourceKey: meta.webresourceKey,
            });
        }
    }

    return modules;
};

export const groupModulesByResourceKey = (moduleDescriptors: Array<IModuleDescriptor>): Array<IGroupedModuleDescriptor> => {
    const groupedModules = groupBy<IModuleDescriptor>(moduleDescriptors, 'webresourceKey');
    return Object.keys(groupedModules).map(webresourceKey => {
        const modules: Array<IModuleDescriptor> = groupedModules[webresourceKey];
        return {
            modules,
            webresourceKey,
            size: modules.map(m => m.size).reduce((a, b) => a + b, 0),
        };
    });
};

export const getAllResourceKeysOfModules = (moduleDescriptors: Array<IModuleDescriptor>): Array<string> => {
    return Array.from(new Set(moduleDescriptors.map(module => module.webresourceKey)));
};
