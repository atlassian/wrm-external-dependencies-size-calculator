import fs from 'fs-extra';
import path from 'path';

import { createChunkDescriptors, IEntrypointDescriptor, IEntrypointWebresourceDependenciesDescriptor } from './chunk-analyzer';
import { generateDependencyUsageMappings, IEntryResourceDependencies, EntryDependencyDescriptor } from './dependency-usage-generator';
import { generateSizeDictionary, ISizeDictionary } from './size-dictionary';
import { generateExternalSize, IEntryChunkDictionary } from './external-size-dictionary';

const CURRENT_FOLDER = process.cwd();
const DESTINATION_FOLDER = path.join(CURRENT_FOLDER, '.wrm-analyzer-reports');

function saveToFile(filePath: string, data: object) {
    fs.writeFileSync(filePath, JSON.stringify(data, null, 4), 'utf-8');
}

interface IReport {
    name: string;
    data: object
}

function saveReportsToFile(reportsMap: Array<IReport>) {
    for (const report of reportsMap) {
        saveToFile(path.join(DESTINATION_FOLDER, `${report.name}.json`), report.data);
    }
}

function mapToArray(map: Map<string, object>) {
    return Array.from(map.values());
}

function mapToObject(map: Map<string, object>) {
    return Array.from(map.entries()).reduce((acc, [key, value]) => {
        acc[key] = value;

        return acc;
    }, {} as { [key: string]: object });
}

function ensureReportDirectory(destinationFolder: string) {
    if (!fs.existsSync(destinationFolder)) {
        fs.mkdirSync(destinationFolder);
    }
}

interface IGeneratedReports {
    entryPoints: Array<IEntrypointWebresourceDependenciesDescriptor>;
    sizes: ISizeDictionary,
    externalSizes: Array<IEntryChunkDictionary>,
    dependencies: EntryDependencyDescriptor;
}

interface IBaseOptions {
    verbose: boolean;
}

interface IPrintableGeneratedReports {
    entryPoints: object,
    sizes: object,
    externalSizes: object,
    dependencies: object,
}

export class ExternalDependenciesSizeCalculator {
    verbose: boolean = false;

    constructor(options: IBaseOptions) {
        if (options) {
            this.verbose = options.verbose;
        }
    }

    print(msg: string) {
        if (this.verbose) {
            console.log(msg);
        }
    }

    exportReportsToFolder(reports: IGeneratedReports, destinationFolder: string) {
        this.print(`Creating reports directory at "${destinationFolder}"`);
        ensureReportDirectory(destinationFolder);
    
        this.print('Writing reports to file.')
        const printableReport = this.toPrintable(reports);
        saveReportsToFile([
            {
                name: 'entry-points-reports',
                data: printableReport.entryPoints,
            },
            {
                name: 'sizes-report',
                data: printableReport.sizes,
            },
            {
                name: 'external-sizes-report',
                data: printableReport.externalSizes,
            },
            {
                name: 'dependencies-report',
                data: printableReport.dependencies,
            }
        ]);
    
        this.print(`Written reports to files is done. You can find them at "${destinationFolder}".`)
    }

    toPrintable(reports: IGeneratedReports): IPrintableGeneratedReports {
        this.print('Turning your report into a JSON.');
    
        return {
            entryPoints: reports.entryPoints,
            sizes: {
                direct: mapToObject(reports.sizes.combined),
                all: mapToObject(reports.sizes.atomic),
            },
            externalSizes: reports.externalSizes,
            dependencies: mapToArray(reports.dependencies).map((d: IEntryResourceDependencies) => ({
                ...d,
                async: mapToArray(d.async),
            }))
        }
    }
    
    async generateReports(WRMReport: Array<IEntrypointDescriptor>, baseUrl?: string): Promise<IGeneratedReports> {
        const chunkDescriptor = await createChunkDescriptors(WRMReport, baseUrl);
    
        this.print(`Found ${chunkDescriptor.length} entry points.`);
        this.print('Start analyzing your really necessary dependencies.')
    
        const dependencies = generateDependencyUsageMappings(chunkDescriptor);
    
        this.print('Calculating dependencies sizes.');
    
        const sizesDictionary = generateSizeDictionary(chunkDescriptor);
    
        this.print('Calculating another bunch of dependencies sizes.')
    
        const externalSizesDictionary = generateExternalSize(
            WRMReport,
            dependencies,
            sizesDictionary
        );
    
        return {
            entryPoints: chunkDescriptor,
            sizes: sizesDictionary,
            externalSizes: externalSizesDictionary,
            dependencies: dependencies
        }
    }

    async calculateReport(WRMReport: Array<IEntrypointDescriptor>, baseUrl?: string): Promise<IGeneratedReports> {
        this.print('Carefully started analyzing your report.');
    
        return await this.generateReports(WRMReport, baseUrl);
    }

    async writeReportsToDirectory(WRMReport: Array<IEntrypointDescriptor>, destination = DESTINATION_FOLDER, baseUrl?: string) {
        this.print('Carefully started analyzing your report.');
    
        const reports = await this.generateReports(WRMReport, baseUrl);
        return this.exportReportsToFolder(reports, destination);
    }
}

