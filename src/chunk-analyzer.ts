import fetch from 'node-fetch';
import url from 'url';
import path from 'path';
import chalk from 'chalk';

import { memoize } from 'lodash';

import { getAllResourceKeysOfModules, groupModulesByResourceKey, parseWRMBatchFile, IGroupedModuleDescriptor } from './wrm-batch-file-parser';
import {deduplicate} from "./utils";

export type WebresourceKey = string;
export type ChunkName = string;
type ModuleName = string;
type FileSize = string;

enum FileType {
    css,
    js,
}

// {baseurl}/download/contextbatch/{css|js}/{contexts/resources}/batch.{css|js}
const getBatchUrlByFileType = (
    webresourceKey: WebresourceKey,
    fileType: FileType,
    baseUrl: string
): string => {
    const resourcePath = `/download/contextbatch/${FileType[fileType]}/${webresourceKey}/batch.${FileType[fileType]}`;
    const baseUrlPathname = url.parse(baseUrl).pathname || '/';
    const fullResourcePath = path.join(baseUrlPathname, resourcePath);
    return url.resolve(baseUrl, fullResourcePath);
};

const fetchBatchFile = async (webresourceKey: WebresourceKey, baseUrl: string) => {
    const batchJsUrl = getBatchUrlByFileType(webresourceKey, FileType.js, baseUrl);
    const batchCssUrl = getBatchUrlByFileType(webresourceKey, FileType.css, baseUrl);
    const jsResource = await fetch(batchJsUrl)
        .then(response => response.text())
        .catch(() => {
            console.log(chalk.red(`Can't fetch file ${batchJsUrl}. Check your "baseUrl" settings and instance availability.`));
            return null;
        });
    const cssResource = await fetch(batchCssUrl)
        .then(response => response.text())
        .catch(() => {
            console.log(chalk.red(`Can't fetch file ${batchCssUrl}. Check your "baseUrl" settings and instance availability.`));
            return null;
        });

    return {
        css: cssResource,
        js: jsResource,
    };
};


export interface IWebresourceSubDependencyResult {
    webresourceKey: WebresourceKey;
    css: {
        grouped: Array<IGroupedModuleDescriptor>;
        resources: Array<WebresourceKey>;
    };
    js: {
        grouped: Array<IGroupedModuleDescriptor>;
        resources: Array<WebresourceKey>;
    };
}

const createMemoizedBatchFileDescriptor = (baseUrl: string) => memoize((webresourceKey: WebresourceKey): Promise<IWebresourceSubDependencyResult> => {
    return fetchBatchFile(webresourceKey, baseUrl)
        .then(({ js, css }) => {
            return {
                css: parseWRMBatchFile(css),
                js: parseWRMBatchFile(js),
            };
        })
        .then(({ js, css }) => {
            return {
                webresourceKey,
                css: {
                    grouped: groupModulesByResourceKey(css),
                    resources: getAllResourceKeysOfModules(css),
                },
                js: {
                    grouped: groupModulesByResourceKey(js),
                    resources: getAllResourceKeysOfModules(js),
                },
            };
        });
});

interface IDependencyUser {
    dependency: WebresourceKey;
    users: Array<ModuleName>;
}

interface IProvidedDependencyUser extends IDependencyUser {
    name: ModuleName;
}

interface IProvidedExternalDependency {
    [key: string]: Array<ModuleName>;
}

export interface IChunkGroupDescriptor {
    name: ChunkName;
    size: FileSize;
    sizeJs: FileSize;
    sizeCss: FileSize;
    syncSize: FileSize;
    syncSizeJs: FileSize;
    syncSizeCss: FileSize;
    asyncSize: FileSize;
    asyncSizeJs: FileSize;
    asyncSizeCss: FileSize;
    asyncRatio: FileSize;
    externalDependencies: Array<WebresourceKey>;
    externalDependenciesUsers: Array<IDependencyUser>;
    providedExternalDependencies: IProvidedExternalDependency;
    providedExternalDependenciesUsers: Array<IProvidedDependencyUser>;
    asyncExternalDependencies: Array<WebresourceKey>;
    asyncExternalDependenciesUsers: Array<IDependencyUser>;
    asyncProvidedExternalDependencies: IProvidedExternalDependency;
    asyncProvidedExternalDependenciesUsers: Array<IProvidedDependencyUser>;
}

export interface IEntrypointDescriptor extends IChunkGroupDescriptor {
    asyncChunks: Array<IChunkGroupDescriptor>;
}

const getWebresourceKeysForChunkGroup = (
    chunkGroup: IChunkGroupDescriptor,
    async: boolean = false
): Array<WebresourceKey> => {
    const dependencies = [...chunkGroup.externalDependencies, ...Object.keys(chunkGroup.providedExternalDependencies)];

    if (async) {
        dependencies.push(...chunkGroup.asyncExternalDependencies);
        dependencies.push(...Object.keys(chunkGroup.asyncProvidedExternalDependencies));
    }

    return deduplicate(dependencies);
};

interface IChunkGroupWebresourceKeysDescriptor {
    name: ChunkName;
    webresourceKeys: Array<WebresourceKey>;
}

interface IEntrypointWebresourceKeysDescriptor extends IChunkGroupWebresourceKeysDescriptor {
    async: Array<IChunkGroupWebresourceKeysDescriptor>;
}

export interface IChunkGroupWebresourceDependenciesDescriptor {
    name: ChunkName;
    result: Array<IWebresourceSubDependencyResult>;
}

export interface IEntrypointWebresourceDependenciesDescriptor extends IChunkGroupWebresourceDependenciesDescriptor {
    async: Array<IChunkGroupWebresourceDependenciesDescriptor>;
}

const getEntrypointWebresourceKeysDescriptor = (
    entryPointDescriptor: IEntrypointDescriptor
): IEntrypointWebresourceKeysDescriptor => {
    const entryPointResourceKeys = {
        name: entryPointDescriptor.name,
        webresourceKeys: getWebresourceKeysForChunkGroup(entryPointDescriptor),
    };
    const asyncWebresourceKeys = entryPointDescriptor.asyncChunks.map(asyncChunkGroup => {
        return {
            name: asyncChunkGroup.name,
            webresourceKeys: getWebresourceKeysForChunkGroup(asyncChunkGroup, true),
        };
    });
    return {
        ...entryPointResourceKeys,
        async: asyncWebresourceKeys,
    };
};

export const createChunkDescriptors = (entryDescriptors: Array<IEntrypointDescriptor>, baseUrl: string = 'https://bulldog.internal.atlassian.com/') => {
    const entrypointWebresourceKeyDescriptors = entryDescriptors.map(getEntrypointWebresourceKeysDescriptor);

    return Promise.all<IEntrypointWebresourceDependenciesDescriptor>(
        entrypointWebresourceKeyDescriptors.map(async entrypointDescriptor => {
            const entrypointWebresourceAndDeps = [entrypointDescriptor.name, ...entrypointDescriptor.webresourceKeys];
            const entrypointResult = await Promise.all<IWebresourceSubDependencyResult>(
                entrypointWebresourceAndDeps.map(createMemoizedBatchFileDescriptor(baseUrl))
            );

            const asyncResults = await Promise.all<IChunkGroupWebresourceDependenciesDescriptor>(
                entrypointDescriptor.async.map(async asyncChunkGroupDescriptor => {
                    const result = await Promise.all<IWebresourceSubDependencyResult>(
                        asyncChunkGroupDescriptor.webresourceKeys.map(createMemoizedBatchFileDescriptor(baseUrl))
                    );
                    return {
                        name: asyncChunkGroupDescriptor.name,
                        result,
                    };
                })
            );

            return {
                name: entrypointDescriptor.name,
                result: entrypointResult,
                async: asyncResults,
            };
        })
    );
};
